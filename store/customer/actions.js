const actions = {
  async signUp({ commit }, payload) {
    const response = await this.$axios.$post(
      '/api/auth/customer-sign-up',
      payload
    )
    return response.data
  },
  async totalCommissions({ commit }) {
    return await this.$axios.$get('/api/customer/total-commission')
  },
}
export default actions
