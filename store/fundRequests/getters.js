const getters = {
  all({ fundRequests }) {
    return fundRequests
  },
  single({ fundRequest }) {
    return fundRequest
  },
}

export default getters
