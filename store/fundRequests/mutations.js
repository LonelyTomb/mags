import { SET_FUNDS, SET_FUND } from './_types'

const mutations = {
  [SET_FUNDS](state, fundRequests) {
    state.fundRequests = { ...fundRequests }
  },
  [SET_FUND](state, fundRequest) {
    state.fundRequest = fundRequest
  },
}

export default mutations
