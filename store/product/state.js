const state = () => ({
  product: null,
  products: {},
  subscription: {},
  subscriptions: {},
})
export default state
