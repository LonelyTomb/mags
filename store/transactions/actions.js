import { SET_TRANSACTION, SET_TRANSACTIONS } from './_types'

const actions = {
  async all({ commit }) {
    const response = await this.$axios.$get('/api/txtn')
    commit(SET_TRANSACTIONS, response.payload)
  },
  async single({ commit }, payload) {
    const response = await this.$axios.$get(`/api/txtn/${payload}`)
    commit(SET_TRANSACTION, response.payload)
  },
  async topUp({ commit }, payload) {
    return await this.$axios.$post(`/api/txtn/fund-wallet`, payload)
    // commit(SET_TRANSACTION, response.payload)
  },
  async subscribeToProduct({ commit }, payload) {
    return await this.$axios.$post(`/api/txtn/product-sub-payment`, payload)
    // commit(SET_TRANSACTION, response.payload)
  },
}

export default actions
