const getters = {
  all({ transactions }) {
    return transactions
  },
  single({ transaction }) {
    return transaction
  },
}

export default getters
