import { SET_TRANSACTIONS, SET_TRANSACTION } from './_types'
const mutations = {
  [SET_TRANSACTIONS](state, transactions) {
    state.transactions = { ...transactions }
  },
  [SET_TRANSACTION](state, transaction) {
    state.transaction = transaction
  },
}

export default mutations
